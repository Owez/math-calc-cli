use math_calc::{parse_str, ErrorKind};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "math_calc")]
struct Opt {
    /// The expression to execute
    #[structopt(short, long)]
    expression: String,

    /// Outputs raw result instead of a formatted string.
    #[structopt(short, long)]
    raw_out: bool,
}

fn main() {
    let opt = Opt::from_args();
    let got_result = parse_str(&opt.expression);

    if opt.raw_out {
        println!("{:?}", got_result);
    } else {
        match got_result {
            Ok(x) => {
                let mut buf = String::new(); // buffer used for print payload

                // Add all results to the formatted buffer
                for result in x.iter().enumerate() {
                    buf.push_str(&format!("Result #{}: {}\n", result.0, result.1));
                }

                buf.remove(buf.len() - 1); // Remove last `\n`

                println!("Calculation result:\n\n{}", buf);
            }
            Err(ErrorKind::InvalidInputEncoding(_)) => println!(
                "Could not parse '{}'! Invalid string formatting..",
                opt.expression
            ),
            Err(ErrorKind::ParseError(e)) => println!("!! Expression parsing error:\n\n{:?}", e),
        }
    }
}
