# math-calc-cli

## About

This is a simple CLI for "Arithmetic Calculator"; allowing use of a quick calculator with universal syntax across multiple platforms.

## Screenshots

![Help command](https://i.imgur.com/N4Iz3LK.png)
![Parsed expression `1 + 1`](https://i.imgur.com/sdllKiA.png)
